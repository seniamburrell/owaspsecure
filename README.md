# About this Project

This work was created to as POC to see how difficult it would be to create test cases that checked for OWASP Top vulnerabilities. The test cases only test 4 CWEs and can be extended to look for more.

# Project Setup

- Install the dependencies in the requirements.txt file
- execute a test using the command `pytest --site_url http://x.x.com/` where --site_url is the URL to the application you want to test
- The test will log the results in a file name `automation_<current_date>.log`
