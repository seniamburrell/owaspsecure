"""
@package utilities

CheckPoint class implementation
It provides functionality to assert the result

Example:
    self.check_point.markFinal("Test Name", result, "Message")
"""
import utilities.custom_logger as cl
import logging
from base.locator import Locator
from traceback import print_stack


# noinspection PyBroadException
class Status(Locator):
    log = cl.custom_logger(logging.INFO)

    def __init__(self, driver):
        """
        Init CheckPoint class
        """
        super(Status, self).__init__(driver)
        self.resultList = []

    def set_result(self, result, result_message):
        try:
            if result is not None:
                if result:
                    self.resultList.append("PASS")
                    self.log.info("### VERIFICATION SUCCESSFUL :: + " + result_message)
                else:
                    self.resultList.append("FAIL")
                    self.log.error("### VERIFICATION FAILED :: + " + result_message)
            else:
                self.resultList.append("FAIL")
                self.log.error("### VERIFICATION FAILED :: + " + result_message)
        except Exception as e:
            self.resultList.append("FAIL")
            self.log.error("### Exception Occurred !!!")
            print(e)

    def mark(self, result, result_message):
        """
        Mark the result of the verification point in a test case
        """
        self.set_result(result, result_message)

    def mark_final(self, test_name, result, result_message):
        """
        Mark the final result of the verification point in a test case
        This needs to be called at least once in a test case
        This should be final test status of the test case
        """
        self.log.info(f"LOGGING RESULTS FOR {test_name}")
        self.set_result(result, result_message)

        # try:
        if "FAIL" in self.resultList:
            self.log.error(test_name + " " + " ### TEST FAILED\n")
            self.resultList.clear()

            try:
                assert result is True
            except AssertionError as e:
                print(e)
        else:
            print(test_name + " ### TEST SUCCESSFUL")
            self.log.info(test_name + " ### TEST SUCCESSFUL\n")
            self.resultList.clear()
            assert result is True
