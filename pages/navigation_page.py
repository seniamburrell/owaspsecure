import utilities.custom_logger as cl
import logging
from base.basepage import BasePage


class NavigationPage(BasePage):

    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver, link_text):
        super().__init__(driver)
        self.driver = driver
        self.link_text = link_text

    def navigate_to_page(self):
        self.element_click(locator=self.link_text, locator_type="link")
