import utilities.custom_logger as cl
from base.locator import Locator
from pages.navigation_page import NavigationPage
from selenium.webdriver.support.select import Select
import logging
from base.basepage import BasePage


class Page(BasePage):

    log = cl.custom_logger(logging.DEBUG)
    _submit = "//input[@type='submit']|//button[@type='submit']"
    _next = "//button[contains(text(),'Next')]"

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver
        # self.nav = NavigationPage(driver)

    # def login(self):
    #     if "login" in self.driver.title

    def verify_cwe201_input(self, command):
        inputs = self.get_element_list("input")
        dropdowns = self.get_element_list("select")
        found = False
        attribute_list = ["email", "phone", "number"]
        string_list = ["test@gmail.com", "8979456789", "2"]

        if inputs:
            for input_field in inputs:
                self.send_keys(data=command, element=input_field)

            self.element_click(self._submit, locator_type="xpath")
            return self.wait_for_alert()

        return True

    def verify_cwe1275_url(self, command):
        self.log.info(f"attaching {command} to the url")
        self.driver.get(self.driver.current_url + command)

        return self.wait_for_alert()

    def verify_cwe_23_path_traversal(self, command):
        xpaths = ["//*[text()='Index of']", "//*[text()='root']"]
        element = None
        result = True

        self.driver.get(self.driver.current_url + command)
        self.log.info(f"attach {command} to the url")

        for css in xpaths:
            element = self.get_element(css)

            if element:
                result = False
                break

        return result

    def verify_cwe20_injection(self, command):
        # enter the command in the input field and click submit
        inputs = self.get_element_list("input")
        _is_submit = None

        if inputs:
            for input_field in inputs:
                self.send_keys(data=command, element=input_field)

            self.element_click(self._submit, locator_type="xpath")
            element = self.get_element("//*[contains(text(),'error')]//*[contains(text(),'failed')]")

            if not element:
                return False

        return True
