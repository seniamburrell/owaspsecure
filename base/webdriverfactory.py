"""
@package base

WebDriver Factory class implementation
It creates a webdriver instance

Example:
    wdf = WebDriverFactory(browser)
    wdf.getWebDriverInstance()
"""
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


class WebDriverFactory:

    def __init__(self, site_url):
        """
        Init WebDriverFactory class

        Returns:
            None
        """
        # self.opts = Options()
        # self.opts.add_argument('--headless')
        # self.opts.add_argument('--disable-gpu')

        self.url = site_url
        self.s = Service(executable_path=ChromeDriverManager().install())

    def get_web_driver_instance(self):
        """
       Get WebDriver Instance based on the browser configuration

        Returns:
            'WebDriver Instance'
        """

        # driver = webdriver.Chrome(service=self.s, chrome_options=self.opts)
        driver = webdriver.Chrome(service=self.s)
        driver.implicitly_wait(10)
        driver.maximize_window()
        # Setting Driver Implicit Time out for An Element
        driver.implicitly_wait(3)
        # Maximize the window
        driver.maximize_window()
        # Loading browser with App URL
        driver.get(self.url)
        driver.set_page_load_timeout(30)

        return driver
