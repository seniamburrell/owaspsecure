import unittest
import pytest
import utilities.custom_logger as cl
import logging
from urllib.parse import urlparse
from pages.page import Page
from utilities.status import Status
import time

# noinspection PyBroadException
@pytest.mark.usefixtures("one_time_setup")
class TestPage(unittest.TestCase):
    log = cl.custom_logger(logging.DEBUG)

    @pytest.fixture(autouse=True)
    def objectSetup(self, one_time_setup):
        self.page = Page(self.driver)
        self.test_status = Status(self.driver)
        # time.sleep(120)
        _links = self.page.get_element_list(locator="//a[@href]", locator_type="xpath")

        self.landing_page_url = self.driver.current_url
        self.app_domain = urlparse(self.landing_page_url).netloc
        self.link_urls = []
        self.total_vulnerabilities = 0
        self.page_vul = 0
        self.message = None
        self.result = None
        self.title = ""

        self.get_link_url(_links)

    def get_link_url(self, links):
        self.log.info(f"\n TOTAL PAGES {len(links)}")
        # loop the links and get the url
        for link in links:
            url = link.get_attribute('href')

            # check if url we are adding is already in the list
            # if it's not add it
            if url not in self.link_urls:
                self.link_urls.append(url)

    def init_log(self):
        self.log.info("*#" * 20)
        self.log.info(f"Test Case {self.test_name} started")
        self.log.info("*#" * 20)

    def log_result(self, url):
        cwe_url = f"https://cwe.mitre.org/data/definitions/{url}"
        message = f"This page is susceptible to {self.test_name} For more information look at {cwe_url}"
        is_success = False

        # log the results
        if self.result:
            self.message = f"{self.title} is not vulnerable to {self.test_name}"
            is_success = True
        else:
            self.total_vulnerabilities += 1
            self.page_vul += 1
            self.message = message
            is_success = False

        # update the test status
        self.test_status.mark_final(self.test_name, is_success, self.message)

    def close_alert(self):
        try:
            alert = self.driver.switch_to.alert
            alert.accept()
        except:
            print('no alert is present')

    def cwe201(self):
        # set the command and message
        self.command = "><script>alert('Hacked');</script>"
        self.message = "A01:2021 Broken Access Control CWE-201 Exposure of Sensitive Information Through Sent Data"
        self.test_name = "CWE-201"

        # add header to separate the logged information
        self.init_log()

        # send the command for each input field
        self.result = self.page.verify_cwe201_input(self.command)

        # verify if the command was executed and log the results
        self.log_result("201.html")

    def cwe1275(self):
        # set the command and message
        self.command = "<script>alert('Hacked');</script>"
        self.message = "A01:2021 Broken Access Control CWE-1275 Sensitive Cookie with Improper SameSite Attribute"
        self.test_name = "CWE-1275"

        # add header to separate the logged information
        self.init_log()

        # test the url of the current page
        self.result = self.page.verify_cwe1275_url(self.command)
        self.log_result("1275.html")

        self.close_alert()

        # reload the page for the next test case
        self.driver.get(self.current_page)

    def cwe_23(self):
        commands = ["?file=../../../../etc/passwd", "?file=/etc/passwd", "?file=var/www/html/etc/passwd", "?file=var"
                                                                                                          "/www/html"]
        # set the command and message
        self.message = "A01:2021 Broken Access Control CWE-23 Relative Path Traversal"
        self.test_name = "CWE23"

        # add header to separate the logged information
        self.init_log()

        # iterate the command and check for each
        for command in commands:
            self.command = command
            self.result = self.page.verify_cwe_23_path_traversal(command)

            self.close_alert()

            # reload the page so the command isn't at the end
            self.driver.get(self.current_page)

            if not self.result:
                break

        self.log_result("23.html")

    def cwe_20(self):
        # set the command and message
        self.command = "admin' --"
        self.message = "A03:201 Injection CWE-20 Improper Input Validation"
        self.test_name = "CWE-20"

        # add header to separate the logged information
        self.init_log()

        # test the url of the current page
        self.result = self.page.verify_cwe20_injection(self.command)
        self.log_result("20.html")

    def execute_tests(self):
        self.title = self.page.get_title()
        self.current_page = self.driver.current_url
        self.page_vul = 0

        # log the name of the page being tested
        self.log.info(f"Testing {self.title} page")
        # self.log.info(f"Page url {self.current_page.replace(self.app_domain, '*')} \n")
        self.log.info(f"Page url {self.current_page} \n")

        # run the test for CWEs
        self.cwe201()

        self.cwe1275()
        self.close_alert()

        self.cwe_23()
        self.close_alert()

        self.cwe_20()

        self.log.info(f"\n Total vulnerabilities found on this page {self.page_vul}")

    def test_app(self):
        self.execute_tests()

        # iterate links list, navigate to each url and test the pages
        for url in self.link_urls:
            # get the domain for the link
            # we don't want to test pages outside the domain
            _link_domain = urlparse(url).netloc

            if _link_domain == self.app_domain:
                # compare with the landing page url
                # we don't want to test it twice
                if self.landing_page_url != url:
                    self.driver.get(url)
                    self.execute_tests()

        self.log.info(f"\n Total overall vulnerabilities {self.total_vulnerabilities}")

