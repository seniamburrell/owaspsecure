import pytest
from base.webdriverfactory import WebDriverFactory


@pytest.fixture(scope="class")
def one_time_setup(request, site_url):
    wdf = WebDriverFactory(site_url)
    driver = wdf.get_web_driver_instance()

    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()


def pytest_addoption(parser):
    parser.addoption("--site_url", help="The url of the application to test e.g., https://www.google.com")
    parser.addoption("--login_username")
    parser.addoption("--login_password")


@pytest.fixture(scope="session")
def site_url(request):
    return request.config.getoption("--site_url")


@pytest.fixture(scope="session")
def login_username(request):
    return request.config.getoption("--login_username")


@pytest.fixture(scope="session")
def login_password(request):
    return request.config.getoption("--login_password")
